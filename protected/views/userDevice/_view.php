<?php
/* @var $this UserDeviceController */
/* @var $data UserDevice */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('UserIndex')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->UserIndex), array('view', 'id'=>$data->UserIndex)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DeviceId')); ?>:</b>
	<?php echo CHtml::encode($data->DeviceId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Serial')); ?>:</b>
	<?php echo CHtml::encode($data->Serial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CurrentVersion')); ?>:</b>
	<?php echo CHtml::encode($data->CurrentVersion); ?>
	<br />


</div>