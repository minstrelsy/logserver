<?php

/**
 * This is the model class for table "UserDeviceTable".
 *
 * The followings are the available columns in table 'UserDeviceTable':
 * @property integer $UserIndex
 * @property integer $DeviceId
 * @property string $Serial
 * @property string $CurrentVersion
 */
class UserDevice extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserDevice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'UserDeviceTable';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('DeviceId, Serial, CurrentVersion', 'required'),
			array('UserIndex, DeviceId', 'numerical', 'integerOnly'=>true),
			array('Serial, CurrentVersion', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('UserIndex, DeviceId, Serial, CurrentVersion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'UserIndex' => 'User Index',
			'DeviceId' => 'Device',
			'Serial' => 'Serial',
			'CurrentVersion' => 'Current Version',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('UserIndex',$this->UserIndex);
		$criteria->compare('DeviceId',$this->DeviceId);
		$criteria->compare('Serial',$this->Serial,true);
		$criteria->compare('CurrentVersion',$this->CurrentVersion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}