<?php

class m130726_082253_UserDeviceTable extends CDbMigration
{
	/*
	public function up()
	{
	}

	public function down()
	{
		echo "m130726_082253_UserDeviceTable does not support migration down.\n";
		return false;
	}
	*/

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable('UserDeviceTable', array(
			'UserIndex' => 'integer',
			'DeviceId' => 'integer NOT NULL',
			'Serial' => 'string NOT NULL',
			'CurrentVersion' => 'string NOT NULL',
			'PRIMARY KEY (UserIndex)',
			'KEY (DeviceId)'
		));
	}

	public function safeDown()
	{
		$this->dropTable('UserDeviceTable');
	}
}
