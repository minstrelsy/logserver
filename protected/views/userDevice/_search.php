<?php
/* @var $this UserDeviceController */
/* @var $model UserDevice */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'UserIndex'); ?>
		<?php echo $form->textField($model,'UserIndex'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DeviceId'); ?>
		<?php echo $form->textField($model,'DeviceId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Serial'); ?>
		<?php echo $form->textField($model,'Serial',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CurrentVersion'); ?>
		<?php echo $form->textField($model,'CurrentVersion',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->