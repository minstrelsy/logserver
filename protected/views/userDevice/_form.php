<?php
/* @var $this UserDeviceController */
/* @var $model UserDevice */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-device-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'UserIndex'); ?>
		<?php echo $form->textField($model,'UserIndex'); ?>
		<?php echo $form->error($model,'UserIndex'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DeviceId'); ?>
		<?php echo $form->textField($model,'DeviceId'); ?>
		<?php echo $form->error($model,'DeviceId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Serial'); ?>
		<?php echo $form->textField($model,'Serial',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Serial'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CurrentVersion'); ?>
		<?php echo $form->textField($model,'CurrentVersion',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'CurrentVersion'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->