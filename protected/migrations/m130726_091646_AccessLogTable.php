<?php

class m130726_091646_AccessLogTable extends CDbMigration
{
	/*
	public function up()
	{
	}

	public function down()
	{
		echo "m130726_091646_AccessLogTable does not support migration down.\n";
		return false;
	}
	*/

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable('AccessLogTable', array(
			'UserIndex' => 'integer NOT NULL',
			'Ip' => 'string NOT NULL',
			'LastAccessTime' => 'timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP'
		));
	}

	public function safeDown()
	{
		$this->dropTable('AccessLogTable');
	}
}
